package org.integratedmodelling.mca;

import java.util.List;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.exceptions.KlabException;

public interface IStakeholder {

    IDirectObservation getSubject();

    /**
     * Return all alternatives retained, making sure everything is initialized.
     * @return alternatives at transition
     */
    List<IAlternative> getAlternatives(ITransition transition);

    void rankAlternatives(ITransition transition) throws KlabException;

    boolean canValue(IAlternative alternative);

}
