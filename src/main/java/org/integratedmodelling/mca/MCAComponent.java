/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.components.Component;
import org.integratedmodelling.api.components.Initialize;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.GeoNS;

@Component(id = "im.mca", version = Version.CURRENT)
public class MCAComponent {

	public static final String INFO_CLASS = "info:mca";

	public static class NS extends GeoNS {

		public static IConcept STAKEHOLDER_ROLE;
		public static IConcept COST_CRITERION_ROLE;
		public static IConcept BENEFIT_CRITERION_ROLE;
		public static IConcept ALTERNATIVE_ROLE;
		public static IConcept CHOICE;
		public static IConcept CONCORDANCE;

		public static boolean synchronize() {

			if (!GeoNS.synchronize()) {
				return false;
			}

			if (STAKEHOLDER_ROLE == null) {

				try {
					STAKEHOLDER_ROLE = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.STAKEHOLDER"), IConcept.class);
					COST_CRITERION_ROLE = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.COST_CRITERION"),
							IConcept.class);
					BENEFIT_CRITERION_ROLE = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.BENEFIT_CRITERION"),
							IConcept.class);
					ALTERNATIVE_ROLE = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.ALTERNATIVE"), IConcept.class);
					CHOICE = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.CHOICE"), IConcept.class);
					CONCORDANCE = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.CONCORDANCE"), IConcept.class);

				} catch (Throwable e) {
					return false;
				}
			}
			return true;
		}
	}

	@Initialize
	public boolean initialize() {
		try {
			NS.synchronize();
		} catch (Throwable e) {
			return false;
		}
		return true;
	}
}
