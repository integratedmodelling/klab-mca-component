/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca.electre3.model;

/**
 *
 * @author Edwin Boaz Soenaryo
 */
public class AAPair implements Comparable<Object> {

    public AAPair(Alternative alt1, Alternative alt2) {
        this.alt1 = alt1;
        this.alt2 = alt2;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof AAPair) {
            AAPair aao = (AAPair) o;
            if (aao.getAlt1() == alt1 && aao.getAlt2() == alt2) {
                return 0;
            }
        }
        return -1;
    }

    public Alternative getAlt1() {
        return alt1;
    }

    public void setAlt1(Alternative alt1) {
        this.alt1 = alt1;
    }

    public Alternative getAlt2() {
        return alt2;
    }

    public void setAlt2(Alternative alt2) {
        this.alt2 = alt2;
    }

    @Override
    public boolean equals(Object anObject) {
        if (this.compareTo(anObject) == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.alt1 != null ? this.alt1.hashCode() : 0);
        hash = 97 * hash + (this.alt2 != null ? this.alt2.hashCode() : 0);
        return hash;
    }

    private Alternative alt1;
    private Alternative alt2;

}
