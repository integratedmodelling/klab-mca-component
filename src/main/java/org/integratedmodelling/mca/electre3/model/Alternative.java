/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca.electre3.model;

import java.io.Serializable;
import java.util.Hashtable;

/**
 *
 * @author Edwin Boaz Soenaryo
 */
public class Alternative implements Serializable {

    private static final long serialVersionUID = 895818478737969333L;

    public Alternative(String name) {
        setName(name);
        setDescription("");
        performances = new Hashtable<Criterion, Double>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCriterionPerformance(Criterion criterion, double value) {
        performances.put(criterion, value);
    }

    public double getCriterionPerformance(Criterion criterion) {
        if (performances.containsKey(criterion)) {
            return performances.get(criterion);
        } else {
            return 0.0;
        }
    }

    public double getPreferenceThreshold(Criterion criterion) {
        double performanceValue = getCriterionPerformance(criterion);
        return criterion.getPreferenceThreshold(performanceValue);
    }

    public double getIndifferenceThreshold(Criterion criterion) {
        double performanceValue = getCriterionPerformance(criterion);
        return criterion.getIndifferenceThreshold(performanceValue);
    }

    public double getVetoThreshold(Criterion criterion) {
        double performanceValue = getCriterionPerformance(criterion);
        return criterion.getVetoThreshold(performanceValue);
    }

    @Override
    public String toString() {
        return name;
    }

    private String                       name;
    private String                       description;
    private Hashtable<Criterion, Double> performances;
}
