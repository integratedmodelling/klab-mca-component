/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca.electre3.model;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author Edwin Boaz Soenaryo
 */
public class CredibilityComputer {

    public CredibilityComputer() {
        credibility = new MatrixModel();
    }

    public void computeCredibility(LinkedList<Criterion> criteria, LinkedList<Alternative> alternatives,
            MatrixModel globalConcordance, Hashtable<Criterion, MatrixModel> indiDiscordance) {

        credibility = new MatrixModel();

        for (Iterator<Alternative> ia1 = alternatives.iterator(); ia1.hasNext();) {
            Alternative a1 = ia1.next();
            for (Iterator<Alternative> ia2 = alternatives.iterator(); ia2.hasNext();) {
                Alternative a2 = ia2.next();
                AAPair aa = new AAPair(a1, a2);
                double gci = globalConcordance.getValue(aa);
                double strength = 1.0d;
                for (Iterator<Criterion> ic = criteria.iterator(); ic.hasNext();) {
                    Criterion c = ic.next();
                    double cdi = indiDiscordance.get(c).getValue(aa);
                    if (cdi > gci) {
                        strength *= (1.0d - cdi) / (1.0d - gci);
                    }
                }
                double s = gci * strength;
                credibility.setValue(a1, a2, s);
            }
        }
    }

    public MatrixModel getCredibility() {
        return credibility;
    }

    private MatrixModel credibility;

}
