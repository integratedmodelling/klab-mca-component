/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca.electre3.store;

import java.io.Serializable;
import java.util.LinkedList;

import org.integratedmodelling.mca.electre3.model.Alternative;
import org.integratedmodelling.mca.electre3.model.Criterion;

/**
 *
 * @author Edwin Boaz Soenaryo
 */
public class StorageBox implements Serializable {

    private static final long serialVersionUID = 3117826326217824360L;

    public StorageBox() {
        alternatives = null;
        criteria = null;
    }

    public LinkedList<Alternative> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(LinkedList<Alternative> alternatives) {
        this.alternatives = alternatives;
    }

    public LinkedList<Criterion> getCriteria() {
        return criteria;
    }

    public void setCriteria(LinkedList<Criterion> criteria) {
        this.criteria = criteria;
    }

    private LinkedList<Alternative> alternatives;
    private LinkedList<Criterion> criteria;

}
