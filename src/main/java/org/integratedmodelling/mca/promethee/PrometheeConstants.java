/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca.promethee;

/**
 *
 * @author Edwin Boaz Soenaryo
 */
public class PrometheeConstants {

    /* Types of criteria */
    public static int USUAL = 101;
    public static int QUASI = 102;
    public static int LINEAR = 103;
    public static int LEVEL = 104;
    public static int QUASILINEAR = 105;
    public static int GAUSSIAN = 106;

    /* Action to do on a criterion */
    public static int MAXIMIZE = 201;
    public static int MINIMIZE = 202;

    /* Ranking method */
    public static int PROMETHEE_I = 301;
    public static int PROMETHEE_II = 302;
}
