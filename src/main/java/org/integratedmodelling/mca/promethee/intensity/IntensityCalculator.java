/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca.promethee.intensity;

import org.integratedmodelling.mca.promethee.PrometheeConstants;

/**
 *
 * @author Edwin Boaz Soenaryo
 */
public class IntensityCalculator {

    public double[][] getIntensities(double[] performanceValues, int operation, HFunctionContext hFunction) {

        int acnt = performanceValues.length; // No of alternatives
        double[][] intensities = new double[acnt][acnt];

        // Loop through pairs of alternatives
        for (int ia1 = 0; ia1 < acnt; ia1++) {
            double pa1 = performanceValues[ia1];
            for (int ia2 = 0; ia2 < acnt; ia2++) {
                double pa2 = performanceValues[ia2];
                double margin = calculateMargin(pa1, pa2, operation);
                if (margin >= 0.0)
                    intensities[ia1][ia2] = hFunction.getValue(margin);
                else
                    intensities[ia1][ia2] = 0.0;
            }
        }

        return intensities;
    }

    private double calculateMargin(double pa1, double pa2, int operation) {
        if (operation == PrometheeConstants.MAXIMIZE) {
            return (pa1 - pa2);
        } else if (operation == PrometheeConstants.MINIMIZE) {
            return (pa2 - pa1);
        } else {
            throw new UnsupportedOperationException("Invalid operation code");
        }
    }
}
