/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca.promethee.ranking;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author Edwin Boaz Soenaryo
 */
public class PrometheeIIRanking implements IRankingStrategy {

    public int[] getRanks(double[] leavingFlows, double[] enteringFlows) {
        int cnt = leavingFlows.length;
        LinkedList<SortableValue> list = new LinkedList<SortableValue>();
        int[] ranks = new int[cnt];

        // Calculate net flows
        double[] netFlows = new double[cnt];
        for (int i = 0; i < cnt; i++) {
            netFlows[i] = leavingFlows[i] - enteringFlows[i];
            list.add(new SortableValue(netFlows[i], i));
        }
        Collections.sort(list);

        int curRank = 1;
        double curValue = Double.NEGATIVE_INFINITY;
        for (Iterator<SortableValue> il = list.iterator(); il.hasNext();) {
            SortableValue sv = il.next();
            ranks[sv.originalPos] = curRank;
            if (curValue != sv.value)
                curRank++;
        }

        return ranks;
    }

    class SortableValue implements Comparable {
        double value;
        int originalPos;

        public SortableValue(double value, int originalPos) {
            this.value = value;
            this.originalPos = originalPos;
        }

        public int compareTo(Object o) {
            if (o instanceof SortableValue) {
                SortableValue so = (SortableValue) o;
                if (so.value > this.value)
                    return 1;
                else if (so.value < this.value)
                    return -1;
                else
                    return 0;
            } else {
                return 0;
            }
        }
    }

}
