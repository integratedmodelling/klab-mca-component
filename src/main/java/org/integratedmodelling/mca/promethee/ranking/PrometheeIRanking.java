/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca.promethee.ranking;

/**
 *
 * @author Edwin Boaz Soenaryo
 */
public class PrometheeIRanking implements IRankingStrategy {

    public int[] getRanks(double[] leavingFlows, double[] enteringFlows) {
        int cnt = leavingFlows.length;

        // Build the outranking relations
        int[][] relations = new int[cnt][cnt];
        int[] outranker = new int[cnt];
        for (int i = 0; i < cnt; i++) {
            for (int j = i; j < cnt; j++) {
                if ((leavingFlows[i] > leavingFlows[j]) && (enteringFlows[i] <= enteringFlows[j])) {
                    relations[i][j] = OUTRANKS;
                    relations[j][i] = OUTRANKED;
                } else if ((leavingFlows[j] > leavingFlows[i]) && (enteringFlows[j] <= enteringFlows[i])) {
                    relations[i][j] = OUTRANKED;
                    relations[j][i] = OUTRANKS;
                } else if ((leavingFlows[i] == leavingFlows[j]) && (enteringFlows[i] == enteringFlows[j])) {
                    relations[i][j] = relations[j][i] = INDIFFERENT;
                } else if ((leavingFlows[i] > leavingFlows[j]) && (enteringFlows[i] > enteringFlows[j])) {
                    relations[i][j] = relations[j][i] = INCOMPARABLE;
                }
            }
        }

        for (int i = 0; i < cnt; i++) {
            for (int j = 0; j < cnt; j++) {
                if (relations[i][j] == OUTRANKED)
                    outranker[i]++;
            }
        }

        // Calculate the ranks
        int[] ranks = new int[cnt];
        boolean finished = false;
        int curRank = 1;
        while (!finished) {
            finished = true;
            int[] newOutranker = new int[cnt];
            System.arraycopy(outranker, 0, newOutranker, 0, cnt);
            for (int i = 0; i < cnt; i++) {
                if (outranker[i] == 0) {
                    newOutranker[i] = -1;
                    finished = false;
                    ranks[i] = curRank;
                    for (int j = 0; j < cnt; j++) {
                        if (relations[i][j] == OUTRANKS)
                            newOutranker[j]--;
                    }
                }
            }
            outranker = newOutranker;
            curRank++;
        }

        return ranks;
    }

    private final int OUTRANKS = 1;
    private final int OUTRANKED = 2;
    private final int INDIFFERENT = 3;
    private final int INCOMPARABLE = 4;

}
