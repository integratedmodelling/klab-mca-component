/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca.promethee.ranking;

/**
 *
 * @author Edwin Boaz Soenaryo
 */
public class RankCalculator {

    public int[] getRanks(double[][] globalIntensities, RankingContext rc) {
        int cnt = globalIntensities.length;
        leavingFlows = new double[cnt];
        enteringFlows = new double[cnt];
        for (int i = 0; i < cnt; i++) {
            for (int j = 0; j < cnt; j++) {
                leavingFlows[i] += globalIntensities[i][j];
                enteringFlows[j] += globalIntensities[i][j];
            }
        }
        return rc.getRanks(leavingFlows, enteringFlows);
    }

    public double[] getEnteringFlows() {
        return enteringFlows;
    }

    public double[] getLeavingFlows() {
        return leavingFlows;
    }

    private double[] leavingFlows;
    private double[] enteringFlows;

}
