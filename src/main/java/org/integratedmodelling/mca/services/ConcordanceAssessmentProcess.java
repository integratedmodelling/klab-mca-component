/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.mca.services;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.IProcessContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.mca.Assessment;
import org.integratedmodelling.mca.MCA;
import org.integratedmodelling.mca.MCAComponent;

/**
 * Any MCA-driven decision process that makes choices based on evaluating criteria. Will look for
 * pairwise and/or absolute values for all the inputs that have a criterion role, and produce a
 * priority ranking for them.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(
        id = "mca.concordance-assessment",
        args = {
                "# levels",
                Prototype.INT,
                "# method",
                "PROMETHEE|EVAMIX|ELECTRE3"
        },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class ConcordanceAssessmentProcess implements IProcessContextualizer {

    private IObservableSemantics concordance;
    private String               concordanceId;

    private MCA.Method           method  = MCA.Method.EVAMIX;
    private IScale               scale;
    private boolean              canDispose;
    private IResolutionScope     resolutionContext;
    private IDirectObservation   context;
    private IProcess             process;
    private IMonitor             monitor;
    private Assessment           assessment;
    private int                  nLevels = 5;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) {
        if (parameters.containsKey("method")) {
            switch (parameters.get("method").toString().toUpperCase()) {
            case "EVAMIX":
                method = MCA.Method.EVAMIX;
                break;
            case "ELECTRE3":
                method = MCA.Method.ELECTRE3;
                break;
            case "PROMETHEE":
                method = MCA.Method.PROMETHEE;
                break;
            }
        }
        if (parameters.containsKey("levels")) {
            this.nLevels = ((Number) parameters.get("levels")).intValue();
        }
    }

    @Override
    public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation context, IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {

        this.context = context;
        this.process = process;
        this.scale = context.getScale();
        this.monitor = monitor;

        MCAComponent.NS.synchronize();

        this.resolutionContext = resolutionContext;

        canDispose = !this.scale.isTemporallyDistributed();

        /*
        * lookup the concordance output observable
        */
        for (String s : expectedOutputs.keySet()) {
            if (expectedOutputs.get(s).is(MCAComponent.NS.CONCORDANCE)) {
                this.concordance = expectedOutputs.get(s);
                this.concordanceId = s;
            }
        }

        if (this.concordance == null && expectedOutputs.size() == 1
                && NS.isQuality(expectedOutputs
                        .get(expectedOutputs.keySet().iterator().next()))) {
            this.concordance = expectedOutputs
                    .get(expectedOutputs.keySet().iterator().next());
            this.concordanceId = expectedOutputs.keySet().iterator().next();
            monitor.info("choosing " + NS.getDisplayName(this.concordance.getType())
                    + " as concordance output", Messages.INFOCLASS_MODEL);
        }

        if (this.concordance == null) {
            monitor.error("cannot establish concordance output: no output of model "
                    + process.getModel().getName() + " is a concordance");
        }

        Map<String, IObservation> ret = new HashMap<>();

        this.assessment = new Assessment(process, context, concordance, resolutionContext, monitor);

        if (assessment.isError()) {
            return ret;
        }

        this.assessment.setDiscretizationLevel(nLevels);

        Map<String, IObservation> result = assessment.compute(null);

        return result == null ? ret : result;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws KlabException {

        canDispose = transition.isLast();
        Map<String, IObservation> result = assessment.compute(transition);

        return result == null ? new HashMap<>() : result;

    }

    private boolean inputsHaveChanged(Map<String, IState> inputs) {
        // TODO Auto-generated method stub
        return false;
    }

    private boolean criteriaHaveChanged(Map<String, IState> inputs) {
        // TODO Auto-generated method stub
        return false;
    }

}
